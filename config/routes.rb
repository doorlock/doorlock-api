Rails.application.routes.draw do
  root to: "errors#routing"

  namespace :api, :constraints => { :format => 'json' } do
    namespace :v1 do
      resources :registrations, only: :none do
        post :sign_in, on: :collection
      end

      resources :doors, only: [:index]

      resources :users, only: :none do
        get :me, on: :collection
      end
    end
  end

  get    "*a", to: "errors#routing"
  post   "*a", to: "errors#routing"
  put    "*a", to: "errors#routing"
  delete "*a", to: "errors#routing"
end
