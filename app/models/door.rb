# == Schema Information
#
# Table name: doors
#
#  id         :integer          not null, primary key
#  name       :integer
#  address    :integer
#  ip         :integer
#  created_at :datetime
#  updated_at :datetime
#

class Door < ActiveRecord::Base
  validates :name, :ip, :address, presence: true

  has_and_belongs_to_many :users
end
