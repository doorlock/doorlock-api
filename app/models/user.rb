# == Schema Information
#
# Table name: users
#
#  id                   :integer          not null, primary key
#  name                 :string
#  email                :string
#  password_digest      :string
#  authentication_token :string
#  created_at           :datetime
#  updated_at           :datetime
#

class User < ActiveRecord::Base
  has_secure_password

  before_create :generate_authentication_token

  validates :name, presence: true
  validates :email, presence: true, uniqueness: true

  has_and_belongs_to_many :doors

  private

  def generate_authentication_token
    self.authentication_token = SecureRandom.hex
  end
end
