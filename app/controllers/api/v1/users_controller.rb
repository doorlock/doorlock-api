class Api::V1::UsersController < API::V1::BaseController
  def me
    @user = current_user
    render_template "users/show"
  end
end
