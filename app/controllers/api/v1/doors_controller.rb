class Api::V1::DoorsController < API::V1::BaseController
  def index
    @doors = current_user.doors
    render_template "doors/index"
  end
end
