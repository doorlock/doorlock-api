class Api::V1::RegistrationsController < API::V1::BaseController
  skip_before_filter :authenticate_user_from_token!

  def sign_in
    if user
      render_template "users/show", status: 201
    else
      fail UnauthenticatedError.new
    end
  end

  private

  def user
    @user ||= User.find_by(email: user_params[:email])
  end

  def user_params
    params.require(:user).permit(:email, :password)
  end
end
