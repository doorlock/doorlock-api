class ApplicationController < ActionController::API
  before_filter -> { request.format = :json }

  rescue_from ApplicationError do |e|
    @status, @class, @message = e.status, e.class, e.message
    render "layouts/error", status: e.status
  end
end
