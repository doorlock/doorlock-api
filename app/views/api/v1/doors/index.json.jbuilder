json.doors do
  json.array! @doors do |door|
    json.door do
      json.name door.name
      json.address door.address
      json.ip door.ip
    end
  end
end
