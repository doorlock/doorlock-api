json.user do
  json.name @user.name
  json.email @user.email
  json.authentication_token @user.authentication_token
end
