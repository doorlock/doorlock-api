json.error do
  json.message @message
  json.class @class
  json.status @status
end
