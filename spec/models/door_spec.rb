# == Schema Information
#
# Table name: doors
#
#  id         :integer          not null, primary key
#  name       :integer
#  address    :integer
#  ip         :integer
#  created_at :datetime
#  updated_at :datetime
#

require "rails_helper"

RSpec.describe Door, type: :model do
  context "Validations" do
    it { should validate_presence_of :name }
    it { should validate_presence_of :address }
    it { should validate_presence_of :ip }
  end
end
