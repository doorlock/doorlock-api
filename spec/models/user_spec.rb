# == Schema Information
#
# Table name: users
#
#  id                   :integer          not null, primary key
#  name                 :string
#  email                :string
#  password_digest      :string
#  authentication_token :string
#  created_at           :datetime
#  updated_at           :datetime
#

require "rails_helper"

RSpec.describe User, type: :model do
  context "Validations" do
    it { should validate_presence_of :email }
    it { should validate_presence_of :name }

    it { should validate_uniqueness_of :email }
  end

  context "Callbacks" do
    describe "#password" do
      let!(:user) { create :user }

      it "creates an password_digest" do
        expect(user.password_digest.size).to eq(60)
      end
    end

    describe "#authentication_token" do
      let!(:user) { build(:user) }

      before do
        expect(user.authentication_token).to be_nil
        user.save!
      end

      it "generate secure random to access token" do
        expect(user.authentication_token).to_not be_nil
      end
    end
  end
end
