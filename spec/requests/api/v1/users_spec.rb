require "rails_helper"

RSpec.describe "Api::V1::Users API", type: :request do
  it_behaves_like("fail auth with token", "get", "/api/v1/users/me")

  context "Returns logged user" do
    describe "GET #me" do
      let!(:user) { create(:user) }
      let!(:env) { { "HTTP_AUTHORIZATION" => "Token #{user.authentication_token}" } }
      before do
        get "/api/v1/users/me", {}, env
      end

      it "returns a successful response" do
        expect(response).to be_success
      end

      it "returns a user" do
        expect(JSON.parse(response.body)).to eq(
          "user" => {
            "name" => user.name,
            "email" => user.email,
            "authentication_token" => user.authentication_token
          }
        )
      end
    end
  end

  context "Invalid user_email" do
    describe "GET #me" do
      let!(:user) { create(:user) }
      before do
        get "/api/v1/users/me", {}, {}
      end

      it "returns a unsuccessful response" do
        expect(response).to_not be_success
      end

      it "returns a user" do
        expect(JSON.parse(response.body)).to eq(
          "error" => {
            "message" => "Your access info aren't correct.",
            "class" => "UnauthenticatedError",
            "status" => 401
          })
      end
    end
  end
end
