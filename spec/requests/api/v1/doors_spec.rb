require "rails_helper"

RSpec.describe "Api::V1::Doors API", type: :request do
  it_behaves_like("fail auth with token", "get", "/api/v1/doors")

  context "Returns doors of logged user" do
    describe "GET #index" do
      let!(:user) { create(:user) }
      let!(:door_one) { create(:door, name: "One", ip: "192.168.0.1") }
      let!(:door_two) { create(:door, name: "Two", ip: "192.168.0.2") }
      let!(:env) { { "HTTP_AUTHORIZATION" => "Token #{user.authentication_token}" } }
      before do
        user.doors << door_one
        user.doors << door_two
        get "/api/v1/doors", {}, env
      end

      it "returns a successful response" do
        expect(response).to be_success
      end

      it "returns a user" do
        expect(JSON.parse(response.body)).to eq(
          "doors" => [
            {"door"=>{"name"=>"One", "address"=>"Rua uruguai 1403 AP 306 A", "ip"=>"192.168.0.1"}},
            {"door"=>{"name"=>"Two", "address"=>"Rua uruguai 1403 AP 306 A", "ip"=>"192.168.0.2"}}
          ]
        )
      end
    end
  end
end
