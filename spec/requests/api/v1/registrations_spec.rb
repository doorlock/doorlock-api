require "rails_helper"

RSpec.describe "Api::V1::Registration API", type: :request do
  context "Login with user" do
    describe "POST #sign_in" do
      let!(:user) { create(:user) }
      before do
        post "/api/v1/registrations/sign_in", {
          user: { email: user.email, password: "123456" }
        } , {}
      end

      it "returns a successful response" do
        expect(response).to be_success
      end

      it "returns a user" do
        user = User.first!
        expect(JSON.parse(response.body)).to eq(
          "user" => {
            "name" => user.name,
            "email" => user.email,
            "authentication_token" => user.authentication_token
          }
        )
      end
    end
  end
  context "Try login user without email" do
    describe "POST #sign_in" do
      let!(:user) { create(:user) }
      before do
        post "/api/v1/registrations/sign_in", {
          user: { email: nil, password: user.password }
        } , {}
      end

      it "returns a successful response" do
        expect(response).to_not be_success
      end

      it "returns a user" do
        expect(JSON.parse(response.body)).to eq(
          "error" => {
            "message" => "Your access info aren't correct.",
            "class" => "UnauthenticatedError",
            "status" => 401
          })
      end
    end
  end
end
