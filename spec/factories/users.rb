# == Schema Information
#
# Table name: users
#
#  id                   :integer          not null, primary key
#  name                 :string
#  email                :string
#  password_digest      :string
#  authentication_token :string
#  created_at           :datetime
#  updated_at           :datetime
#

FactoryGirl.define do
  factory :user do
    name "User"
    sequence(:email) { |n| "user#{n}@example.com" }
    password "123456"
  end
end
