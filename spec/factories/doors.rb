# == Schema Information
#
# Table name: doors
#
#  id         :integer          not null, primary key
#  name       :integer
#  address    :integer
#  ip         :integer
#  created_at :datetime
#  updated_at :datetime
#

FactoryGirl.define do
  factory :door do
    name "Porta 01"
    address "Rua uruguai 1403 AP 306 A"
    ip "192.168.0.2"
  end
end
