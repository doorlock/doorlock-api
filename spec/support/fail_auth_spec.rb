RSpec.shared_examples "fail auth with token" do |method, path|
  context 'with authentication_token' do
    before do
      send(method, path, { "HTTP_AUTHORIZATION" => "Token token" }, {})
    end

    it "returns a successful response" do
      expect(response).to_not be_success
    end

    it "returns a user" do
      expect(JSON.parse(response.body)).to eq(
        "error" => {
          "message" => "Your access info aren't correct.",
          "class" => "UnauthenticatedError",
          "status" => 401
        })
    end
  end
end
