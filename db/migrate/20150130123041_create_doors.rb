class CreateDoors < ActiveRecord::Migration
  def change
    create_table(:doors) do |t|
      t.string :name
      t.string :address
      t.string :ip
      t.timestamps
    end
    add_index(:doors, [:id], :unique => true)
  end
end
