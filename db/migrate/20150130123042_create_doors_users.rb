class CreateDoorsUsers < ActiveRecord::Migration
  def change
    create_table :doors_users, id: false do |t|
      t.integer :user_id, null: false
      t.integer :door_id, null: false
    end
  end
end
